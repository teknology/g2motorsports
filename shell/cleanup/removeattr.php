<?php
	require_once '../../app/Mage.php';
	Mage::app();
	
	$installer = new Mage_Sales_Model_Resource_Setup();
	$installer->startSetup();
	$installer->updateAttribute('customer', 'dob', 'is_required', 1);

	//$installer->removeAttribute('customer', 'can_log_in');
	$installer->endSetup();
	

/*
	$attributes = Mage::getModel('customer/customer')->getAttributes();
foreach ($attributes as $attr) :
 echo $attr->getAttribute_code() . "<br>";
 endforeach; 
 */